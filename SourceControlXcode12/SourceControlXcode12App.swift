//
//  SourceControlXcode12App.swift
//  SourceControlXcode12
//
//  Created by Collin Lent on 4/7/21.
//

import SwiftUI

@main
struct SourceControlXcode12App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
